(async function() {
    id = new URLSearchParams(document.location.search).get("id");
    const data = await fetchJSON("/2.4.0/index.json");
    filters = Object.keys(data.filters);
    if(filters.indexOf(id) != -1){
        document.getElementById("main").style.visibility = "visible";
        document.getElementById("id").innerText = id;
        document.getElementById("id-code").innerText = id;
        document.getElementById("id-code-2").innerText = id;
        document.getElementById("title").innerText = data.filters[id].title;
        document.getElementById("description").innerText = data.filters[id].desc;
        const source = document.createElement("a");
        source.href = data.filters[id].source;
        source.innerText = source;
        document.getElementById("source").appendChild(source);
        document.getElementById("syntax").innerText = data.filters[id].syntax;
        document.getElementById("mirrors").innerText = data.filters[id].mirrors.length;
        const homepage = document.createElement("a");
        homepage.href = data.filters[id].homepage;
        homepage.innerText = data.filters[id].homepage;
        document.getElementById("homepage").appendChild(homepage);
        const license = document.createElement("a");
        license.href = data.filters[id].license[1];
        license.innerText = data.filters[id].license[0];
        document.getElementById("license").appendChild(license);
        for(tag of data.filters[id].tags){
            document.getElementById("tags").innerText += " ".concat(tag);
        }
        if(data.filters[id].deprecated){
            document.getElementById("deprecated").innerText = "yes";
        }
        else {
            document.getElementById("deprecated").innerText = "no";
        }
    }
    else{
        document.getElementById("error").style.display = "block";
    }
})();